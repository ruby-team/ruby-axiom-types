require 'gem2deb/rake/spectask'

TEST_SEPARATELY = %w[
  spec/unit/axiom/types/class_methods/finalize_spec.rb
]

Gem2Deb::Rake::RSpecTask.new do |spec|
  spec.pattern = Dir['spec/**/*_spec.rb'] - TEST_SEPARATELY
end

Gem2Deb::Rake::RSpecTask.new(:class_methods) do |spec|
  spec.pattern = TEST_SEPARATELY
end

task :default => :class_methods
